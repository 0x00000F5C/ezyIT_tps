<?php
	$listcart = GetCart();
	$selectcurrency = SelectedCurrency();
	$dataresult = GetCartResult($selectcurrency);
    $this->load->model("user/m_user");
	$dealer = $this->m_user->GetUserGroup();
?>
<form id="frmcheckout" name="frmcheckout">
    <input type="hidden" name="item_number" value="">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="cpp_header_image">
    <input type="hidden" name="cpp_logo_image">
    <input type="hidden" name="typepayment" value="paypal">
    <input type="hidden" name="image_url"> 
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="return" value="">
    <input type="hidden" name="cancel_return" value="">
    <input type="hidden" name="business" value="">
    <input type="hidden" name="custom" value=""  >
    <input name="item_name" type="hidden" id="item_name"  size="45">
    <input name="itemidvoucher" type="hidden" id="itemidvoucher"  size="45">
    <input name="amount" type="hidden" id="amount" size="45">
    <input type="hidden" name="no_shipping" value="1">
    <div class="checkout-product">
        <table>
            <thead>
                <tr>
                    <td class="name">Name</td>
                    <td class="model">Model</td>
                    <td class="quantity">Qty</td>
                    <td class="price">Price</td>
                    <td class="total">Total</td>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($listcart) > 0) {
                    foreach ($listcart as $cartsatuan) {
                        ?>
                        <tr>
                            <td class="name"><a href="<?php echo base_url() . 'index.php/user/view_product/' . $cartsatuan['id'] . '?' . $path . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $cartsatuan['name']) . '.html' ?>"><?php echo $cartsatuan['name'] ?></a>
                            </td>
                            <td class="model"><?php echo @$cartsatuan['products_code'] ?>
                            </td>
                            <td class="quantity"><?php echo @$cartsatuan['qty'] ?>
                            </td>
                            <td class="price">
                                <?php if ($cartsatuan['normal_price'] > $cartsatuan['price']) { ?>
                                    <span class="price-old">
                                        <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['normal_price']), $selectcurrency) ?>
                                    </span>
                                <?php } ?>
                                <span class="price-new">
                                    <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['price']), $selectcurrency) ?>
                                </span>

                            </td>
                            <td class="total"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['subtotal']), $selectcurrency) ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="image" colspan="5"> 
                            Your shopping cart is empty!
                        </td>
                    </tr>

                <?php } ?>
            </tbody>
            <tfoot>
				<tr>
					<td colspan="4" class = "price"><b>Dealer discount:</b></td>
					<td class = "total"><?php echo $dealer->percent_off + 0 . "% off"; ?></td>
				</tr>
                <tr>
                    <td colspan="4" class="price"><b>Sub-Total:</b></td>
                    <td class="total" id="chk_subtotal">
                        <?php 
                            $subtotalprice = @$dataresult['totalsum'] - (@$dataresult['totalsum'] * ($dealer->percent_off / 100));
							echo DefaultCurrencyForView($subtotalprice, $selectcurrency);
                        ?>
					</td>
                </tr>
                <tr>
                    <td colspan="4" class="price"><b><?php echo @$name_shipping ?>:</b></td>
                    <td class="total" id="chk_shipcost"><?php echo @$shipprice ?></td>
                </tr>
                <tr>
                    <td colspan="4" class="price"><b>Total:</b></td>
					<td class="total" id="chk_grandtotal"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, @$subtotalprice + $nominalprice), $selectcurrency); ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="payment">
        <div class="buttons">
            <div class="right">
                I have read and agree to the <a class="colorbox cboxElement" href="javascript:;" alt="Terms of Service"><b>Terms of Service</b></a>        <input type="checkbox" name="agree" value="1">      
                <input type="button" value="Continue" id="btt_checkout" class="button">
            </div>
        </div>
    </div>
</form>
<script>
	$("#agree").change(function() {
		if ($("this").is(":checked")) {
			$("#btt_checkout").prop("disabled", "false");
		} else {
			$("#btt_checkout").prop("disabled", "true");
		}
	});

    $("#btt_checkout").click(function () {
        var strconfirmation = '';
        if ($("[name=typepayment]").val() == "credit value") {
            strconfirmation = "Do you want to process this order? The process will reduce your balance";
        } else {
            strconfirmation = "Do you want to process this order? ";
        }


        swal({
            title: "Are you sure?",
            text: strconfirmation,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function () {
            LoadBar.show();
            $.ajax(
                    {
                        url: baseurl + "/index.php/cart/CheckOut",
                        data: $("#frmbillingdetails ,#frmdeliverydetails,#frmcheckout,#frmdeliverymethod,#frmpaymentmethod").serialize() + "&" + $.param({'isusenewaddress': isusenewaddress, 'isusenewdelivery': isusenewdelivery}),
                        dataType: "json",
                        type: "post",
                        success: function (data)
                        {
                            if (data.st)
                            {
                                if (data.jns == "paypal")
                                {
                                    document.frmcheckout.item_name.value = 'Invoice :' + data.custom;
                                    document.frmcheckout.amount.value = data.amount;
                                    document.frmcheckout.itemidvoucher.value = data.custom;
                                    document.frmcheckout.return.value = data.return;
                                    document.frmcheckout.cancel_return.value = "";
                                    document.frmcheckout.custom.value = data.custom;
                                    document.frmcheckout.no_note.value = "";
                                    document.frmcheckout.business.value = data.account_paypal;
                                    document.frmcheckout.currency_code.value = 'SGD';
                                    document.frmcheckout.image_url.value = data.cpp_header_image;
                                    document.frmcheckout.cpp_header_image.value = data.cpp_header_image;
                                    document.frmcheckout.cpp_logo_image.value = data.cpp_header_image;
                                    document.frmcheckout.cancel_return.value = data.cancel;
                                    document.getElementById("frmcheckout").action = data.action;
                                    $("#frmcheckout").submit();
                                }
                                else
                                {
                                    window.location.href = data.return;
                                }
                            }
                            else
                            {
                                LoadBar.hide();
                                messageerror(data.msg);
                            }
                        },
                        error: function (xhr, status, error)
                        {
                            LoadBar.hide();
                            messageerror(xhr.responseText);
                        }
                    });

        });


    })
</script>