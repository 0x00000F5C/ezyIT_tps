<div class="box">
	<br>
	<div class="box-heading">Result Of '<?php echo $keyword?>' :</div>
	 	 <div class="products_container">
			<?php
			$config = GetConfig();
			$selectcurrency = SelectedCurrency();
            if (count($search) > 0) {
                foreach ($search as $product) {
                    ?>

                     <div class="product_holder">
		                <div class="product_holder_inside">	

		                    <div class="image">
		                        <a href="<?php echo base_url() . 'index.php/user/view_product/' . $product->product_id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $product->product_name) . '.html' ?>">
		                            <img class="primary-photo" src="<?php echo $config['folderproduct'] . $product->product_image ?>" alt="<?php echo $product->product_name ?>">
                                    <?php foreach ($product->listimage as $imagestatuan) { ?>
                                       <img class="secondary-photo" src="<?php echo $config['folderproduct'] . $imagestatuan['prodet_image'] ?>">
                                     <?php } ?>
                                    </a></div>
		                        <div class="pr_info">
		                            <!-- ADD BY SAE, 24 Nov 2016 set qty -->
		                             <input type="hidden" name="quantity" class="qty_input" size="2" value="1">
		                            <!--END SAE-->
		                            <div class="name"><a href="<?php echo base_url() . 'index.php/user/view_product/' . $product->product_id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $product->product_name) . '.html' ?>"><?php echo $product->product_name ?></a></div>
		                            <div class="price">
		                                <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $product->product_unit_cost), $selectcurrency) ?>		          		        </div>
		                            <div class="cart"><a  data-id="<?php echo $product->product_id ?>" class="button addcartbtn" onclick="javascript:;" ><span>Add to Cart</span></a></div>
		                        </div>
		                </div>
		            </div>
                   

                    <?php } ?>

                    <?php } else {
                   echo '<div class="articlenotfound">Product Not Found.</div>';
                }
                ?>
          </div>
</div>

<script>
    $(".addcartbtn").unbind("click").on("click", function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/cart/addtocart",
                    data: {
                        product_id: $(this).data("id"),
                        quantity: $('.qty_input').val()

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            modaldialog(data.msg);
                            messagesuccess(data.msg);
                            RefreshCart();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;
    })



</script> 