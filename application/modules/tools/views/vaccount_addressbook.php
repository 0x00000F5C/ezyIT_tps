<?php left_account("address"); ?>
<div id="content"> 
    <div class="breadcrumb">
        <div class="breadcrumb">
            <a href="<?php echo base_url() ?>">Home</a>
            » <a href="<?php echo base_url() . 'index.php/tools/account' ?>">Account</a>
            » Address Book
        </div>
    </div>
    <h1>Address Book</h1>
    <h2>Address Book Entries</h2>

    <?php
    if (count($listaddress) > 0) {

        foreach ($listaddress as $address) {
            ?>
            <div class="content">
                <table style="width: 100%;">
                    <tbody><tr>
                            <td><?php echo $address->first_name . ' ' . $address->last_name ?><br><?php echo @$address->address ?><br><?php echo $address->state_name ?><br><?php echo $address->country_name ?></td>
                            <td style="text-align: right;"><a href="<?php echo base_url() . 'index.php/tools/editaddress/' . $address->id_address_book ?>" class="button">Edit</a> &nbsp; <a href="javascript:;" data-id="<?php echo @$address->id_address_book ?>" class="button btndelete">Delete</a></td>
                        </tr>
                    </tbody></table>
            </div>
        <?php
        }
    } else {
        echo '<br/>No Address For This Account<br/><br/>';
    }
    ?>

    <div class="buttons">
        <div class="left"><a href="<?php echo base_url() . 'index.php/tools/account' ?>" class="button">Back</a></div>
        <div class="right"><a href="<?php echo base_url() . 'index.php/tools/newaddress' ?>" class="button">New Address</a></div>
    </div>
</div>
<script>
    $(".btndelete").click(function () {
        if (confirm("Are you sure want to delete this address?"))
        {
             $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/tools/deleteaddress',
            dataType: 'json',
            data: {
                id_addres:$(this).data("id")
            },
            success: function (data) {
                if (data.st)
                {
                    window.location.reload();
                }
                else
                {
                    messageerror(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });




            return false;
        }

        return false;


    })


</script>