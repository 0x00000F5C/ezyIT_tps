<?php left_account('news'); ?>
<div id="content">  <div class="breadcrumb">
        <a href="<?php echo base_url() ?>">Home</a>
        » <a href="<?php echo base_url() . 'index.php/tools/account' ?>">Account</a>
        »Newsletter
    </div>
    <h1>Newsletter Subscription</h1>
    <form id="frmsubsription" method="post" enctype="multipart/form-data">
        <div class="content">
            <table class="form">
                <tbody><tr>
                        <td>Subscribe:</td>
                        <td>
                            <input type="hidden" name="email" value="<?php echo @$mail?>" />
                            <input type="radio" id="rd_yes" name="newsletter" value="1" checked="checked" >
                            <label for="rd_yes">Yes&nbsp;</label>
                            <input id="rd_no" type="radio" name="newsletter" value="0" <?php echo $issuscribe=='0'?'checked="checked"':''?>>
                            <label for="rd_no"> No </label>           </td>
                    </tr>
                </tbody></table>
        </div>
        <div class="buttons">
            <div class="left"><a href="<?php echo base_url() . 'index.php/tools/account' ?>" class="button">Back</a></div>
            <div class="right"><input type="submit" value="Continue" class="button"></div>
        </div>
    </form>
</div>
<script>
    $("form#frmsubsription").submit(function () {
         LoadBar.show();
        $.ajax(
                {
                    url: baseurl + "index.php/tools/subscribe_action",
                    data: $(this).serialize(),
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            window.location.href=baseurl + "index.php/tools/account"
                        }
                        else
                        {
                            messageerror(data.msg);
                        }
                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    },
                    complete:function(){
                         LoadBar.hide();
                        
                    }
                });


        return false;
    })

</script>