<?php
left_account("");
?>

<div id="content">  
    <div class="breadcrumb">
        <div class="breadcrumb">
            <a href="<?php echo base_url() ?>">Home</a>
            » <a href="<?php echo base_url() . 'index.php/tools/account' ?>">Account</a>
            » Address Book
        </div>
    </div>
    <h1>Address Book</h1>
    <form method="post" id="frmaddressbook" enctype="multipart/form-data">
        <h2>Edit Address</h2>
        <div class="content">
            <table class="form">
                <tbody><tr>
                <input type="hidden" id="txt_id_address_book" name="id_address_book" value="<?php echo @$model->id_address_book ?>" />
                <td><span class="required">*</span> First Name:</td>
                <td><input type="text" name="first_name" value="<?php echo @$model->first_name ?>">
                </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Last Name:</td>
                    <td><input type="text" name="last_name" value="<?php echo @$model->last_name ?>">
                    </td>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td><input type="text" name="company" value="<?php echo @$model->company ?>"></td>
                </tr>
                <tr>
                    <td>Company ID:</td>
                    <td><input type="text" name="company_id" value="<?php echo @$model->company_id ?>">
                    </td>
                </tr>
                <tr>
                    <td>Telephone:</td>
                    <td><input type="text" name="telpnum" value="<?php echo @$model->telpnum ?>">
                    </td>
                </tr>
                <tr>
                    <td>Fax:</td>
                    <td><input type="text" name="fax" value="<?php echo @$model->fax ?>">
                    </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Address :</td>
                    <td><textarea type="text" name="address" value=""><?php echo @$model->address ?></textarea>
                    </td>
                </tr>

                <tr>
                    <td><span class="required">*</span> City:</td>
                    <td><input type="text" name="city" value="<?php echo @$model->city ?>">
                    </td>
                </tr>
                <tr>
                    <td><span id="postcode-required" class="required" >*</span> Post Code:</td>
                    <td><input type="text" name="post_code" value="<?php echo @$model->post_code ?>">
                    </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Country:</td>
                    <td><select id="dropdown_country_id" name="country_id" style="width:160px;">
                            <option value="0"> --- Please Select --- </option>
                            <?php foreach ($listcountry as $country) { ?>
                                <option <?php echo $country->kode == @$model->country_id ? "selected" : "" ?> value="<?php echo $country->kode ?>"><?php echo $country->name ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Region / State:</td>
                    <td><select id="dropdown_state_id" name="state_id" style="width:160px;" ><option value="0"> --- Please Select --- </option>
                        </select>
                    </td>
                </tr>
                </tbody></table>
        </div>
        <div class="buttons">
            <div class="left"><a href="<?php echo base_url() . 'index.php/tools/addressbook' ?>" class="button ">Back</a></div>
            <div class="right">
                <input type="submit" value="Continue" class="button">
            </div>
        </div>
    </form>
</div>
<script>

    $(document).ready(function () {
<?php if (!CheckEmpty(@$model->country_id)) { ?>
            RefreshDropDownCountry("#dropdown_country_id", "#dropdown_state_id", <?php echo @$model->state_id == '' ? '0' : @$model->state_id ?>);


<?php } ?>

    })


    $("#dropdown_country_id").change(function () {
        RefreshDropDownCountry("#dropdown_country_id", "#dropdown_state_id", 0);

    })
    $("form#frmaddressbook").submit(function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/tools/addressbookmanipulate",
                    data: $(this).serialize(),
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            window.location.href = baseurl + "index.php/tools/addressbook";
                        }
                        else
                        {
                            messageerror(data.msg);
                        }
                    },
                    error: function (xhr, status, error)
                    {

                        messageerror(xhr.responseText);
                    }
                });


        return false;
    });

</script>

