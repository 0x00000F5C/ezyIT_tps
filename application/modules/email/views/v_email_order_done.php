<style>
    .areaclick:hover a{
        background-color: #fff !important;;
        color:#FF0097 !Important;

    }
    .areaclick td{
        padding:5px;
        
      
        
    }
    .areaclick th{
        background-color: #FF0097;
        color:#fff;
        padding:5px;
        
    }
</style>
<div style="max-width:600px;margin:0 auto;overflow:hidden;border:1px solid #ddd;font-family: 'Century Gothic', CenturyGothic, arial, sans-serif">
    <div style='text-align:center;padding:10px;'>
        <img src='<?php echo base_url() . 'images/logo.png' ?>' style='margin:0 auto;' />
    </div>
    <div style="padding:16px;background-color:#f2f0f3;border-top-style:none;font-size:14px;">
        <p style="font-size:14px;">Dear <?php
            echo @$first_name;
            if (!CheckEmpty(@$last_name)) {
                echo ' , ' . $last_name;
            }
            if (!CheckEmpty(@$company_name)) {
                echo ' (' . @$company_name . ')';
            }
            ?>
            <span style="font-weight:bold;font-style:italic">
                <br>
                <br>
            </span>
        </p>
        <p>
            Your bill was made on <?php echo DefaultTanggalWithday($dateorder) ?> <br/>
        <table style="font-size:14px;">
            <tr>
                <td>Invoice</td>
                <td>:</td>
                <td>#<?php echo @$order_number ?></td>
            </tr>
            <tr>
                <td>Amount Due</td>
                <td>:</td>
                <td><?php echo DefaultCurrencyForView(@$grandtotal + @$shipping) ?></td> 
            </tr>
        </table>
            
        </p>
        <p style="margin-top:20px;border-top:solid 1px #000;padding-top:20px;">
            Your Order Detail
        </p>
        
        <div class='areaclick' style="font-size:12px;font-weight:bold;text-align:center">
            <table style="font-size:12px;width:100%;" >
                <tr>
                    <th>Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                </tr>
                <?php foreach($listdetail as $detail) { ?>
                <tr style="background-color:#fff;">
                    <td><?php echo @$detail->name_product?></td>
                    <td><?php echo @$detail->qty ?> x </td>
                    <td><?php echo DefaultCurrencyForView(@$detail->price) ?></td>
                    <td><?php echo DefaultCurrencyForView(@$detail->subtotal) ?></td>
                </tr>
                
                
                
                <?php } ?>
                <tr style="background-color: #fff;">
                    <td colspan="3" style="text-align:right;" >Total</td>
                    <td><?php echo DefaultCurrencyForView($totalamount)?></td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan="3" style="text-align:right;" >Shipment <?php echo '<b>('.@$name_shipping.')</b>'?></td>
                    <td><?php echo DefaultCurrencyForView($shipping)?></td>
                </tr>
                <tr style="background-color: #fff;">
                    <td colspan="3" style="text-align:right;" >GrandTotal</td>
                    <td><?php echo DefaultCurrencyForView($totalamount+$shipping)?></td>
                </tr>
            </table>
        </div>
        
    </div>
    <div style="padding:10px;text-align:center;font-family:helvetica, sans-serif, arial;font-size:11px;color:#999999;" >
        Copyright © 2015,The Paper Stone. .</div>
</div>