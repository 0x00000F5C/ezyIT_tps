<div id="content" class="inside_page">

 <?php foreach ($article as $tampilkan): ?>
 <div class="breadcrumb">
        <a href="<?php echo base_url() ?>">Home</a>&nbsp;
        <?php if (!CheckEmpty(@$tampilkan)) { ?>
            »&nbsp;&nbsp;<a href="<?php echo base_url() . 'index.php/user/view_article/' . $tampilkan['id_article'] . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $tampilkan['name_article']) . '.html' ?>"><?php echo $tampilkan['name_article'] ?></a>
        <?php } ?>
 </div>
 <?php endforeach; ?>

 <?php foreach ($article as $tampilkan): ?>
 	<p><?php echo $tampilkan['content']; ?></p>
 <?php endforeach; ?>
 </div>
