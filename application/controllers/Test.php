<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	public function test_a() {
		$this->db->from("tbl_ecommerce_customer");
		$this->db->join("tbl_ecommerce_group_customer", "`tbl_ecommerce_customer`.`id_type` = `tbl_ecommerce_group_customer`.`group_customer_id`", "left");
		$this->db->where(array("id_customer_ecommerce" => GetUserId()));
		$user = $this->db->get()->row();
		die(json_encode($user, JSON_PRETTY_PRINT));
	}
	public function test_checkout() {
		$data = array();
		# $this->load->module('user/m_user');
		# $data = GetMembership(GetUserId());
		$this->load->view("testcheckout", $data);
	}
}
