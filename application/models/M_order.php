<?php

class M_order extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function Activation($id) {
        $this->load->model("m_email");
        $this->m_email->SendEmailOrder($id);
        $this->db->update("tbl_ecommerce_order", array("status_order" => 1), array("id_order" => $id));
        $model = $this->m_order->GetOneOrder($id);
        $this->db->delete("tbl_ecommerce_cart", array("id_member" => $model->id_customer_ecommerce));
        
        $this->cart->destroy();
    }

    function GetOneOrder($id) {
        $this->db->from("tbl_ecommerce_order");
        $this->db->join("tbl_ecommerce_address_book delivery", "delivery.id_address_book=tbl_ecommerce_order.id_delivery");
        $this->db->select("tbl_ecommerce_order.*,first_name,last_name,company,name_shipping");
        $this->db->join("tbl_ecommerce_shipping", "tbl_ecommerce_shipping.id_shipping=tbl_ecommerce_order.id_shipment", "left");
        $this->db->where(array("id_order" => $id));
        $row = $this->db->get()->row();
        if ($row != null) {
            $this->db->from("tbl_ecommerce_order_detail");
            $this->db->where(array("id_order" => $id));
            $row->listdetail = $this->db->get()->result();
        }
        return $row;
    }

    function CreateOrder($model) {

        $this->load->model("m_shipping");
        $message = '';
        $config = GetConfig();
        $this->db->trans_begin();
        $data = array();
        $data['st'] = false;
        $dataresult = GetCartResult();
        $order = array();
        $custid = GetUserId();
        $format = 'E-' . date('mY');

        $query = $this->db->query('select ifnull(max(right(order_number,4)),0) as "max" from tbl_ecommerce_order where order_number like "%' . $format . '%"');
        $lastinvoice = $query->row()->max;
        $noinvoice = $format . str_pad($lastinvoice + 1, "5", "0", STR_PAD_LEFT);
        if ($model['isguest'] == 0 && $model['password_billing'] != '') {
            $account = array();
            $account['firstname'] = $model['firstname_billing'];
            $account['lastname'] = $model['lastname_billing'];
            $account['email'] = $model['email_billing'];
            $account['company_id'] = $model['company_id_billing'];
            $account['company'] = $model['company_billing'];
            $account['address'] = $model['address_billing'];
            $account['city'] = $model['city_billing'];
            $account['postcode'] = $model['postcode_billing'];
            $account['telephone'] = $model['telephone_billing'];
            $account['fax'] = $model['fax_billing'];
            $account['country_id'] = $model['country_billing'];
            $account['state_id'] = $model['region_billing'];
            $account['password'] = $model['password_billing'];
            $account['newsletter'] = CheckKey($model, "newsletter") ? $model['newsletter'] : 0;

            $this->load->model("user/m_user");
            $message = $this->m_user->register_account($account, true, false);
            if (strlen($message) < 4) {
                $custid = $message;
            } else {
                $data['msg'] = $message;
                return $data;
                die;
            }
        }
        if (CheckEmpty($custid)) {
            $custid = 3;
        }
        $order['order_number'] = $noinvoice;
        $order['create_date'] = GetDateNow();
        $order['create_by'] = $custid;
        $order['dateorder'] = GetDateNow();
        $order['id_customer_ecommerce'] = $custid;
        $order['status_order'] = 0;
		if (strtolower($model['payment_method']) != "membership") {
			if (!empty($this->session->userdata("coupon_id"))) {
				if ($this->session->userdata("coupon_type") == "1") {
					$order['totalamount'] = $dataresult['totalnominal'] - ($dataresult['totalnominal'] * $this->session->userdata("coupon_value") / 100);
				} elseif ($this->session->userdata("coupon_type") == "2") {
					$order['totalamount'] = $dataresult['totalnominal'] - $this->session->userdata("coupon_value");
				} else {
					$order['totalamount'] = $dataresult['totalnominal'];
				}
			} else {
				$order['totalamount'] = $dataresult['totalnominal'];
			}
		} else {
			if (!empty($this->session->userdata("coupon_id"))) {
				if ($this->session->userdata("coupon_type") == "1") {
					$order['totalamount'] = ($dataresult['totalnominal'] - ($dataresult['totalnominal'] * 10 / 100)) - ($dataresult['totalnominal'] * $this->session->userdata("coupon_value") / 100);
				} elseif ($this->session->userdata("coupon_type") == "2") {
					$order['totalamount'] = ($dataresult['totalnominal'] - ($dataresult['totalnominal'] * 10 / 100)) - $this->session->userdata("coupon_value");
				} else {
					$order['totalamount'] = ($dataresult['totalnominal'] - ($dataresult['totalnominal'] * 10 / 100));
				}
			} else {
				$order['totalamount'] = ($dataresult['totalnominal'] - ($dataresult['totalnominal'] * 10 / 100));
			}
		}
        $order['paymenttype'] = $model['payment_method'];
        $order['coupon'] = (!empty($this->session->userdata("coupon_id"))) ? $this->session->userdata("coupon_id") : "0";
        $order['gst'] = ((float) 10 / (float) 110) * $dataresult['totalnominal'];
        $order['grandtotal'] = $dataresult['totalnominal'] - $order['gst'];
        $order['email'] = $model['email_billing'] == '' ? GetUsername() : $model['email_billing'];
        if (CheckEmpty(GetUserId())) {
            $billing = array();
            $billing['first_name'] = $model['firstname_billing'];
            $billing['last_name'] = $model['lastname_billing'];
            $billing['company'] = $model['company_billing'];
            $billing['company_id'] = $model['company_id_billing'];
            $billing['id_member'] = $custid;
            $billing['address'] = $model['address_billing'];
            $billing['city'] = $model['city_billing'];
            $billing['post_code'] = $model['postcode_billing'];
            $billing['telpnum'] = $model['telephone_billing'];
            $billing['fax'] = $model['fax_billing'];
            $billing['country_id'] = $model['country_billing'];
            $billing['state_id'] = $model['region_billing'];
            $this->db->insert("tbl_ecommerce_address_book", $billing);
            $billingid = $this->db->insert_id();
            $order['id_billing'] = $billingid;
            if ($model['same_address'] == 1) {
                $order['id_delivery'] = $order['id_billing'];
            } else {
                $delivery = array();
                $delivery['first_name'] = $model['firstname_delivery'];
                $delivery['last_name'] = $model['lastname_delivery'];
                $delivery['company'] = $model['company_delivery'];
                $delivery['address'] = $model['address_delivery'];
                $delivery['city'] = $model['city_delivery'];
                $delivery['post_code'] = $model['postcode_delivery'];
                $delivery['telpnum'] = $model['telephone_delivery'];
                $delivery['fax'] = $model['fax_delivery'];
                $delivery['country_id'] = $model['country_delivery'];
                $delivery['state_id'] = $model['region_delivery'];
                $delivery['id_member'] = $custid;
                $this->db->insert("tbl_ecommerce_address_book", $delivery);
                $deliveryid = $this->db->insert_id();
                $order['id_delivery'] = $deliveryid;
            }
        } else {
            $order['id_billing'] = $model['billing_address_id'];
            $order['id_delivery'] = $model['delivery_address_id'];
        }
        $listshipment = $this->m_shipping->GetShipmentForCart(0, 0, $order['id_delivery']);
        $order['shipping'] = 0;
        $order['id_shipment'] = $model['shipping_method'];
        foreach ($listshipment as $ship) {
            if ($ship->id_shipping == $model['shipping_method']) {
                $order['shipping'] = $ship->nominalprice;
            }
        }
        $order['notification_delivery'] = $model['comment'];
        $order['notification_payment'] = $model['comment_payment'];

        $this->db->insert("tbl_ecommerce_order", $order);
        $order_id = $this->db->insert_id();

        $listcart = GetCart();
        foreach ($listcart as $cartssatuan) {
            $satuan = array();
            $satuan['id_order'] = $order_id;
            $satuan['id_product'] = $cartssatuan['id'];
            $satuan['name_product'] = $cartssatuan['name'];
            $satuan['qty'] = $cartssatuan['qty'];
            $satuan['normal_price'] = $cartssatuan['normal_price'];
            $satuan['price'] = $cartssatuan['price'];
            $satuan['subtotal'] = $cartssatuan['price'] * $cartssatuan['qty'];
            $satuan['discount'] = $cartssatuan['normal_price'] - $cartssatuan['price'];
            $this->db->insert("tbl_ecommerce_order_detail", $satuan);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        $datareturn = array();
        $datareturn['jns'] = $model['payment_method'] == 'pp_express' ? 'paypal' : 'credit';
        $datareturn['return'] = base_url() . 'index.php/tools/checkorder/' . $order_id;
        $datareturn['custom'] = $noinvoice;
        $datareturn['account_paypal'] = $config['isproduction'] == '1' ? $config['paypalsandbox'] : $config['paypalproduction'];
        if ($config['isproduction'] != '1') {
            $datareturn['action'] = 'https://www.paypal.com/cgi-bin/webscr';
        } else {
            $datareturn['action'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        }
        $datareturn['amount'] = $order['totalamount'];
        $datareturn['cpp_header_image'] = base_url() . 'images/logopaypal.png';

        $datareturn['complete'] = base_url() . 'index.php/tools/checkorder/' . $order_id;

        $datareturn['complete'] = base_url() . 'index.php/tools/checkordertest/' . $order_id;

        $datareturn['st'] = true;

        $datapaypal = array(
            'METHOD' => 'SetExpressCheckout',
            'RETURNURL' => base_url() . 'index.php/tools/checkorder/' . $order_id,
            'CANCELURL' => base_url() . 'index.php/tools/cancelorder/' . $order_id,
            'REQCONFIRMSHIPPING' => 0,
            'LOCALECODE' => 'EN',
            'LANDINGPAGE' => 'Login',
            'CHANNELTYPE' => 'Merchant',
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'SGD',
            'PAYMENTREQUEST_0_AMT' => $order['totalamount']
        );

        $this->load->model("m_paypal");
        $result = $this->m_paypal->call($datapaypal);
        if (!CheckKey($result, 'TOKEN')) {
            if (CheckKey($result, 'L_LONGMESSAGE0'))
                SetMessageSession(0, $result['L_LONGMESSAGE0']);
            else
                SetMessageSession(0, 'Some Error Occured');
            $this->ReturnBackOrder($order_id);
            $data['url'] = base_url() . 'index/php/user/message';
        }
        else {
            $this->session->set_userdata('paypaltoken', $result['TOKEN']);
        }

        $data['jns'] = 'paypal';
        if ($config['isproduction'] == 1) {
            $data['st'] = true;
            $data['url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $result['TOKEN'];
        } else {
            $data['st'] = true;
            $data['url'] = 'https://www.paypal.com/cgi-bin/webscr?cmd=express-checkout&token=' . $result['TOKEN'];
        }

        $this->session->unset_userdata("coupon_status");
        $this->session->unset_userdata("coupon_serial");
        $this->session->unset_userdata("coupon_value");
        $this->session->unset_userdata("coupon_id");

        return $datareturn;
    }

    function ReturnBackOrder($id_order) {
        
    }

    function GetOneTrans($iv) {
        $this->db->from("tbl_ecommerce_pmembership");
        $this->db->where(array("id_order" => $iv));
        $row = $this->db->get()->row();
        if ($row != null) {
            $this->db->from("tbl_ecommerce_order_detail");
            $this->db->where(array("id_order" => $iv));
            $row->listdetail = $this->db->get()->result();
        }
        return $row;
    }

}

?>