<?php

class M_product extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function GetWishlist() {
        $this->load->model("m_discount");
        $this->db->from("tbl_ecommerce_wish_list");
        $this->db->join("tbl_inv_product_master", "tbl_inv_product_master.product_id=tbl_ecommerce_wish_list.id_product");
        $this->db->join("tbl_inv_code_management", "tbl_inv_code_management.code_id=tbl_inv_product_master.product_code_id", "left");
        $this->db->select("tbl_inv_product_master.*,code_generated");
        $this->db->where(array("id_member" => GetUserId()));
        $listwishlist = $this->db->get()->result();
        $config = GetConfig();
        foreach ($listwishlist as $whislist) {
            $headers = get_headers($config['folderproduct'] . $whislist->product_image);

            if (!stripos($headers[0], "200 OK")) {
                $whislist->product_image = "default.jpg";
            }
            if ($whislist->product_image == '') {
                $whislist->product_image = "default.jpg";
            }
            $whislist->listprice = $this->m_discount->GetDiscountItem($whislist->product_id, true);
        }
        return $listwishlist;
    }

    function WishListDelete($id_product) {
        $this->db->delete("tbl_ecommerce_wish_list", array("id_member" => GetUserId(), "id_product" => $id_product));
        SetMessageSession(1, 'Item successfully removed from wishlist');
    }

    function WishListAdd($id_product) {


        $product = $this->GetOneProduct($id_product);

        $data = array();
        $data['st'] = false;
        if ($product != null) {
            $userid = GetUserId();
            if (CheckEmpty($userid)) {
                $data['msg'] = "Your session has ecpired, try for relogin or reload the page";
            } else {
                $this->db->from("tbl_ecommerce_wish_list");
                $this->db->where(array("id_member" => $userid, "id_product" => $id_product));
                $row = $this->db->get()->row();
                if ($row == null) {

                    $this->db->insert("tbl_ecommerce_wish_list", array("id_member" => $userid, "id_product" => $id_product));
                    $data['msg'] = $product->product_name . ' successfully added to wishlist';
                    $data['st'] = true;
                } else {
                    $data['msg'] = $product->product_name . ' already in your wishlist';
                }
            }
        } else {
            $data['msg'] = 'Product Cannot be found in database<br/>';
        }
        return $data;
    }

    function GetAllFeatureProduct($limit = 0, $isecommerce = true) {
        $this->db->from("tbl_inv_product_master");
        $this->db->where(array("featured" => 1, "product_status !=" => 1));
        $this->db->order_by("RAND()");
        $where = array();

        $where['product_active'] = 1;
        if ($isecommerce) {
            $where['product_status !='] = 3;
        } else {
            $where['product_status !='] = 2;
        }
        if (!CheckEmpty($limit)) {
            $this->db->limit($limit);
        }
        $this->db->where($where);
        $listproduct = $this->db->get()->result();
        $config = GetConfig();
        foreach ($listproduct as $product) {
            $headers = get_headers($config['folderproduct'] . $product->product_image);

            //add by sae, 2016/12/17 - query for get second image product//
            $this->db->from("tbl_inv_product_detail");
            $this->db->where(array("prodet_product_id"=>$product->product_id));
            $listimage=$this->db->get()->result_array();
            
            $product->listimage = $listimage;
            //end sae//

            if (!stripos($headers[0], "200 OK")) {
                $product->product_image = "default.jpg";
            }
            if (@$product->product_image == '') {
                $product->product_image = "default.jpg";
            }
        }
        return $listproduct;
    }

    public function GetOneProduct($id, $val = false) {
        $this->db->from("tbl_inv_product_master");
        $this->db->join("tbl_inv_code_management", "tbl_inv_code_management.code_id=tbl_inv_product_master.product_code_id", "left");
        $this->db->join("tbl_inv_category", "tbl_inv_category.cat_id=tbl_inv_product_master.product_category", "left");
        $this->db->join("tbl_inv_category_sub", "tbl_inv_category_sub.id_sub_category=tbl_inv_product_master.product_sub_category_id", "left");
        $this->db->select("tbl_inv_product_master.*,tbl_inv_code_management.code_prefix,tbl_inv_category_sub.name_sub_category as 'subname',tbl_inv_category.cat_name as 'parentname'");
        $wherearray = array();
        $wherearray['product_id'] = $id;
        if ($val) {
            $wherearray['product_status'] = 1;
        }
        $this->db->where($wherearray);
        $product = $this->db->get()->row();

         //add by sae, 2016/12/17 - query for get second image product//
        $this->db->from("tbl_inv_product_detail");
        $this->db->where(array("prodet_product_id"=>$id));
        $listimage=$this->db->get()->result_array();
        
        if(@$product->product_image!='')
        {
            array_unshift($listimage, array("prodet_image"=>@$product->product_image));
        }
        $product->listimage=$listimage;
        //end sae//

        return $product;
    }

    function GetTotalProducts($cat_id = 0, $sub_cat_id = 0, $headerid = 0, $isecommerce = true) {
        $this->db->from("tbl_inv_product_master");
        $this->db->join("tbl_inv_category_sub", "tbl_inv_product_master.product_sub_category_id=tbl_inv_category_sub.id_sub_category");
        $this->db->where(array("product_status !=" => '1'));
        $where = array();

        $where['product_active'] = 1;
        if ($isecommerce) {
            $where['product_status !='] = 3;
        } else {
            $where['product_status !='] = 2;
        }
        if (!CheckEmpty($headerid)) {
            $where['sub_header_category_id'] = $headerid;
        }
        if (!CheckEmpty($cat_id)) {
            $where['product_category'] = $cat_id;
        }
        if (!CheckEmpty($sub_cat_id)) {
            $where['product_sub_category_id'] = $sub_cat_id;
        }

        $this->db->where($where);
        return $this->db->get()->num_rows();
    }

    function GetListProduct($cat_id = 0, $sub_cat_id = 0, $headerid = 0, $start = 0, $limit = 0, $isecommerce = true) {
        $config = GetConfig();
        $this->load->model("m_discount");
        $this->db->from("tbl_inv_product_master");
        $this->db->join("tbl_inv_category_sub", "tbl_inv_product_master.product_sub_category_id=tbl_inv_category_sub.id_sub_category");
        $this->db->where(array("product_status !=" => '1'));
        $where = array();

        $where['product_active'] = 1;
        if ($isecommerce) {
            $where['product_status !='] = 3;
        } else {
            $where['product_status !='] = 2;
        }
        if (!CheckEmpty($headerid)) {
            $where['sub_header_category_id'] = $headerid;
        }
        if (CheckEmpty($start)) {
            $start = 1;
        }
        if (!CheckEmpty($start) || !CheckEmpty($limit)) {
            $this->db->limit($limit, ($start - 1) * $limit);
        }
        if (!CheckEmpty($cat_id)) {
            $where['product_category'] = $cat_id;
        }
        if (!CheckEmpty($sub_cat_id)) {
            $where['product_sub_category_id'] = $sub_cat_id;
        }
        $this->db->where($where);
        $this->db->select("product_description,id_discount,price_discount,not_group_affect,product_name,product_image,product_id");

        $listproduct = $this->db->get()->result();
        foreach ($listproduct as $product) {
            $headers = get_headers($config['folderproduct'] . $product->product_image);


            //add by sae, 2016/12/17 - query for get second image product//
            $this->db->from("tbl_inv_product_detail");
            $this->db->where(array("prodet_product_id"=>$product->product_id));
            $listimage=$this->db->get()->result_array();
            
            $product->listimage = $listimage;
            //end sae//

            if (!stripos($headers[0], "200 OK")) {
                $product->product_image = "default.jpg";
            }
            $product->listprice = $this->m_discount->GetDiscountItem($product->product_id, $isecommerce);
        }
        return $listproduct;
    }

    public function search_keyword($keyword) {
        $this->db->from("tbl_inv_product_master");
        $this->db->like('product_name', $keyword);
        $listproduct = $this->db->get()->result();

        $config = GetConfig();
        foreach ($listproduct as $product) {
            $headers = get_headers($config['folderproduct'] . $product->product_image);

            $this->db->from("tbl_inv_product_detail");
            $this->db->where(array("prodet_product_id" => $product->product_id));
            $listimage = $this->db->get()->result_array();

            $product->listimage = $listimage;

            if (!stripos($headers[0], "200 OK")) {
                $product->product_image = "default.jpg";
            }
            if (@$product->product_image == '') {
                $product->product_image = "default.jpg";
            }
        }

        return $listproduct;
    }

}

?>